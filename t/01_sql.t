use strict;
use utf8;
use Test::More;
use_ok('CSVquery::SQL');

my $csv_query = CSVquery::SQL->new('t/data');
ok($csv_query, 'new');
isa_ok($csv_query, 'CSVquery::SQL');

my $sth = $csv_query->do("CREATE TABLE test (id INT, name VARCHAR(255))");
isa_ok($sth, 'DBI::st');

$csv_query->do("INSERT INTO test (id, name) VALUES (?,?)", 1, "papaparapapa");
$csv_query->do("INSERT INTO test (id, name) VALUES (?,?)", 2, "papapa rapapa");
$csv_query->do("INSERT INTO test (id, name) VALUES (?,?)", 3, "русский буква");
$csv_query->do("UPDATE test SET name=? WHERE id=1", "lol wat?");

my $rows = $csv_query->do("SELECT name FROM test WHERE id IN (1,3)")->fetchall_arrayref;
is(scalar @$rows, 2, "number of rows fetched");
is($rows->[0][0], 'lol wat?', 'row 1 has proper value');
is($rows->[1][0], 'русский буква', 'row 2 has proper value');

$csv_query->do("DROP TABLE test");

done_testing;
