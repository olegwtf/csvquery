package CSVquery::SQL;

use strict;
use DBI;
use DBD::CSV;
use Carp;

sub new {
	my ($class, $dbpath, %opts) = @_;
	
	croak 'usage: new($dbpath, %opts)' unless defined $dbpath;
	my $self = {};
	$self->{dbh} = DBI->connect(
		'dbi:CSV:', '', '', {
			f_dir        => $dbpath,
			f_ext        => $opts{ext}//'.csv',
			csv_sep_char => $opts{separator}//',',
			f_encoding   => 'utf8',
			RaiseError   => 1,
			PrintError   => 0,
			csv_class    => "Text::CSV_PP",
		}
	);
	
	bless $self, $class;
}

sub do {
	my ($self, $sql, @bind_vars) = @_;
	
	my $sth = $self->{dbh}->prepare($sql);
	$sth->execute(@bind_vars);
	return $sth;
}

sub DESTROY {
	shift->{dbh}->disconnect();
}

1;
