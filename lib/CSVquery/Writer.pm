package CSVquery::Writer;

BEGIN { $ENV{PERL_TEXT_CSV} = 'Text::CSV_PP' }
use strict;
use Text::CSV;
use IO::Handle;

sub new {
	my ($class, $output_path, $separator) = @_;
	
	my $self = {};
	if (defined $output_path) {
		open $self->{fh}, '>:utf8', $output_path or die "`$output_path': $!";
		$self->{fh}->autoflush(1);
	}
	else {
		$self->{fh} = \*STDOUT;
	}
	$self->{csv} = Text::CSV->new({sep_char => $separator//',', eol => "\n", binary => 1, always_quote => 1});
	
	bless $self, $class;
}

sub write {
	my ($self, $row) = @_;
	$self->{csv}->print( $self->{fh}, $row );
}

1;
